(function($, mdepolska) {
    'use strict';

    //var bindInsertAtCaret = function() {
    //    /*
    //     source: http://stackoverflow.com/questions/946534/insert-text-into-textarea-with-jquery/2819568#2819568
    //     */
    //    $.fn.extend({
    //        insertAtCaret: function(myValue) {
    //            if (document.selection) {
    //                this.focus();
    //                var sel = document.selection.createRange();
    //                sel.text = myValue;
    //                this.focus();
    //            }
    //            else if (this.selectionStart || this.selectionStart === '0') {
    //                var startPos = this.selectionStart;
    //                var endPos = this.selectionEnd;
    //                var scrollTop = this.scrollTop;
    //                this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
    //                this.focus();
    //                this.selectionStart = startPos + myValue.length;
    //                this.selectionEnd = startPos + myValue.length;
    //                this.scrollTop = scrollTop;
    //            } else {
    //                this.value += myValue;
    //                this.focus();
    //            }
    //        }
    //    });
    //};

    /*
     globals

     util

     classes (binding in constructor)

     init

     */



    /* ============== popolsku.globals ============== */
    mdepolska.globals = {
        tildaModKeyCode: 192,
        asteriskModKeyCode: 56,
        diacriticChars: 'AaCcEeLlNnOoSsXxZz',
        hasInitialized: false
    };

    /* ============== popolsku.init ============== */

    var addBindings = function(elem, options) {
        options = options || {};
        new mdepolska.classes.Typer(elem);
        // Optionally add this line to initialize Blinkenlichten
        if( options.blinkenlichten ) {
            new mdepolska.classes.Blinkenlichten(elem);
        } else {
            // Don't add Blinkenlichten, but do add decoration, so it is visible
            // that extra functionality is added to this textarea.
            new mdepolska.classes.Decoration(elem);
        }
    };

    /**
     * @function initAll
     * @description initialization after async loading of jQuery
     */
    var initAll = function(options) {
        // Only initialize once; prevent drupal from executing the contents twice, despite the document ready check.
        if( !mdepolska.globals.hasInitialized ) {
            mdepolska.globals.hasInitialized = true;
            // Check if old browser
            if( window.console !== undefined ) {
                // Initialize Polish Diacritical Typer
                // Add Polish typer function to each textarea with class 'polska'
                $('textarea.polska').each( function(){
                    if($(this).is(':not(.initialized)')) {
                        addBindings(this, options);
                    }
                });
                // Also add Polish typer to each new textarea when focussed
                $(document).on('focus', 'textarea, input', function() {
                    //console.log('focussed');
                    if($(this).is(':not(.initialized)')) {
                        addBindings(this, options);
                    }
                });

            } else if( options.oldbrowser ) {
                // If this is an old browser (i.e. doesn't have console) and if the "old browser" option is set, notify the user.
                mdepolska.util.oldBrowser();
            }
        }
    };

    mdepolska.init = function(options) {

        if( typeof $ === 'undefined' ) {
            mdepolska.loadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js?', function() {
                $ = window.jQuery;
                initAll(options);
            });
        } else {
            initAll(options);
        }
    };

    /* ============== mdepolska.classes ============== */
    mdepolska.classes = {};

    /**
     * @class Typer
     * @description
     */
    /** @constructor */
    mdepolska.classes.Typer = function(element) {

        this.prevChar = null;
        this.elem = element;

        this.init();

    };

    mdepolska.classes.Typer.prototype = {
        init: function() {
            var self = this;
            $(this.elem).keydown( function(ev) {
                /*
                 http://freepages.genealogy.rootsweb.ancestry.com/~atpc/learn/tools/pl-alphabet.html
                 special characters: AaCcEeLlNnOoSsZz (Z*z*)
                 If ~ was typed ( keyCode 192 + shiftKey = ~ )
                 Or if * was typed ( keyCode 56 + shiftKey = * )
                 */
                //if( (mdepolska.globals.tildaModKeyCode === e.keyCode && e.shiftKey) ||
                //    (mdepolska.globals.asteriskModKeyCode === e.keyCode && e.shiftKey) ) {

                //if( (mdepolska.globals.tildaModKeyCode === e.keyCode && e.shiftKey) ) {
                //    self.prevChar = self.getPrevChar();
                //    if( mdepolska.globals.diacriticChars.indexOf( self.prevChar ) > -1 ) {
                //        self.replaceChar(e);
                //        return false;
                //    }
                //}
                var typedChar = String.fromCharCode(ev.keyCode);
                if( mdepolska.globals.diacriticChars.indexOf( typedChar ) > -1 ) {
                    self.prevChar = self.getPrevChar();
                    if( '~' === self.prevChar ) {
                        if(!ev.shiftKey) {
                            typedChar = typedChar.toLowerCase();
                        }
                        self.replaceChar(typedChar);
                        return false;
                    }
                }
            });

            $(this.elem).addClass('initialized');
        },

        getPrevChar: function() {
            var val = $(this.elem).val();
            if( val.length > 0 ) {
                return val.substr( this.elem.selectionStart - 1, 1 );
            }
            return null;
        },

        replaceChar: function(char) {
            var newChar = this.mapChar(char);
            $(this.elem).val( $(this.elem).val().substring( 0, this.elem.selectionStart - 1 ) + newChar );
        },

        mapChar: function(char) {//modifier){
            //var char = this.prevChar;
            console.log(char);
            if( true ) {//mdepolska.globals.tildaModKeyCode === modifier ) {
                switch(char) {
                    case 'a': return 'ą';
                    case 'A': return 'Ą';
                    case 'C': return 'Ć';
                    case 'c': return 'ć';
                    case 'E': return 'Ę';
                    case 'e': return 'ę';
                    case 'L': return 'Ł';
                    case 'l': return 'ł';
                    case 'N': return 'Ń';
                    case 'n': return 'ń';
                    case 'O': return 'Ó';
                    case 'o': return 'ó';
                    case 'S': return 'Ś';
                    case 's': return 'ś';
                    case 'X': return 'Ź';
                    case 'x': return 'ź';
                    case 'Z': return 'Ż';
                    case 'z': return 'ż';
                    default: return char;
                }
                //} else if ( mdepolska.globals.asteriskModKeyCode === modifier ) {
                //    switch(char) {
                //        case 'Z': return 'Ż';
                //        case 'z': return 'ż';
                //        default: return char;
                //    }
            }
        }
    };

    /**
     * @class Blinkenlichten
     * @description
     */
    /** @constructor */
    mdepolska.classes.Blinkenlichten = function(element) {
        this.elem = element;

        this.init();
    };

    mdepolska.classes.Blinkenlichten.prototype = {
        init: function() {
            this.createHtml();
            var self = this;
            $(this.elem).keydown( function(e) {
                var licht = self.getLicht(e);
                self.down(licht);
            }).keyup( function() {
                // Clear all blinkenlichten
                $('#blinkenlichten ul li').removeClass('active');
            });
        },

        createHtml: function() {
            $(this.elem).after('<div id="blinkenlichten">'+
                '<ul class="meta">'+
                '  <li id="tilda">~</li>'+
                '  <li id="asterisk">*</li>'+
                '</ul>'+
                '<ul>'+
                '  <li id="a">A</li>'+
                '  <li id="c">C</li>'+
                '  <li id="e">E</li>'+
                '  <li id="l">L</li>'+
                '  <li id="n">N</li>'+
                '  <li id="o">O</li>'+
                '  <li id="s">S</li>'+
                '  <li id="z">Z</li>'+
                '</ul>'+
                '</div>');
        },

        getLicht: function(e) {
            var licht = null;
            var charFromCode = String.fromCharCode( e.keyCode );
            if( (mdepolska.globals.tildaModKeyCode === e.keyCode && e.shiftKey) ) {
                licht = $('#blinkenlichten ul li#tilda');
            } else if( (mdepolska.globals.asteriskModKeyCode === e.keyCode && e.shiftKey) ) {
                licht = $('#blinkenlichten ul li#asterisk');
            } else if( mdepolska.globals.diacriticChars.indexOf( charFromCode ) > -1 ) {
                var charLower = charFromCode.toLowerCase();
                licht = $('#blinkenlichten ul li#' + charLower );
            } else {
                return;
            }
            return licht;
        },

        down: function(licht) {
            if( licht !== undefined ) {
                licht.addClass('active');
            }
        }
    };

    /**
     * @class Decoration
     * @description
     */
    /** @constructor */
    mdepolska.classes.Decoration = function(element) {
        this.elem = element;
        this.bgColor = '#8DB11F';

        this.init();
    };

    mdepolska.classes.Decoration.prototype = {

        init: function() {
            // All inline styling, because it is for the bookmarklet
            $(this.elem).css('border','1px solid ' + this.bgColor);
            //console.log("where to add blokje?" + $(this.elem).offset().left + "," + $(this.elem).offset().top + " -> " + $(this.elem).width() );
            var top = $(this.elem).offset().top;
            var left = $(this.elem).offset().left + $(this.elem).width() + 1; // +1 is for overlapping borders
            this.createTab(top, left);
        },

        createTab: function(top, left) {
            $(this.elem).before('<div class="polska-decoration" style="'+
                'position: absolute;'+
                'background: ' + this.bgColor + ';' +
                'border: 1px solid ' + this.bgColor + ';' +
                'border-top-right-radius: 5px;'+
                'border-bottom-right-radius: 5px;'+
                'color: white;'+
                'cursor: pointer;'+
                'font-weight: bold;'+
                'font-family: sans-serif;'+
                'left: ' + left + 'px;'+
                'padding: 2px;'+
                'top: ' + top + 'px;"'+
                ' title="MDE Polska!"><a href="http://mdworld.nl/cms/content/polska" style="text-decoration: none; color: white;">P</a></div>');
        }

    };

    /* ============== mdepolska.util ============== */
    mdepolska.util = {};

    mdepolska.util.oldBrowser = function() {
        $('textarea.polska')
            .attr('disabled','disabled')
            .addClass('readonly')
            .addClass('initialized')
            .val('THIS IS AN OLD BROWSER, TYPER IS DISABLED.');
    };

    window.mdepolska = mdepolska;
})(window.jQuery, window.mdepolska || {});