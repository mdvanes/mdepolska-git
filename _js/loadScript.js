(function(mdepolska) {
    'use strict';

    // http://stackoverflow.com/questions/11212809/load-jquery-dynamically
    mdepolska.loadScript = function(url, success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
            done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState ||
                this.readyState === 'loaded' ||
                this.readyState === 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                //head.removeChild(script);
            }
        };
        head.appendChild(script);
    };
    //getScript('http://code.jquery.com/jquery-latest.min.js',function() {
    //    // Yay jQuery is ready \o/
    //});​

    // source: http://www.sitepoint.com/dynamically-load-jquery-library-javascript/
    //mdepolska.loadScript = function(url, callback) {
    //    var script = document.createElement('script');
    //    script.type = 'text/javascript';
    //
    //    if (script.readyState) { //IE
    //        script.onreadystatechange = function () {
    //            if (script.readyState === 'loaded' || script.readyState === 'complete') {
    //                script.onreadystatechange = null;
    //                //callback();
    //            }
    //        };
    //    } else { //Others
    //        script.onload = function () {
    //            setTimeout(function() {
    //                console.log(typeof $, $);
    //                callback();
    //            }, 500);
    //        };
    //    }
    //
    //    script.src = url;
    //    document.getElementsByTagName('head')[0].appendChild(script);
    //};

    window.mdepolska = mdepolska;

    //loadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', function () {
    //
    //    //jQuery loaded
    //    console.log('jquery loaded');
    //
    //});


})(window.mdepolska || {});