/*! popolsku 0.3.0 2016-02-11 12:30 */
(function(mdepolska) {
    "use strict";
    mdepolska.loadScript = function(url, success) {
        var script = document.createElement("script");
        script.src = url;
        var head = document.getElementsByTagName("head")[0], done = false;
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
            }
        };
        head.appendChild(script);
    };
    window.mdepolska = mdepolska;
})(window.mdepolska || {});

(function($, mdepolska) {
    "use strict";
    mdepolska.globals = {
        tildaModKeyCode: 192,
        asteriskModKeyCode: 56,
        diacriticChars: "AaCcEeLlNnOoSsXxZz",
        hasInitialized: false
    };
    var addBindings = function(elem, options) {
        options = options || {};
        new mdepolska.classes.Typer(elem);
        if (options.blinkenlichten) {
            new mdepolska.classes.Blinkenlichten(elem);
        } else {
            new mdepolska.classes.Decoration(elem);
        }
    };
    var initAll = function(options) {
        if (!mdepolska.globals.hasInitialized) {
            mdepolska.globals.hasInitialized = true;
            if (window.console !== undefined) {
                $("textarea.polska").each(function() {
                    if ($(this).is(":not(.initialized)")) {
                        addBindings(this, options);
                    }
                });
                $(document).on("focus", "textarea, input", function() {
                    if ($(this).is(":not(.initialized)")) {
                        addBindings(this, options);
                    }
                });
            } else if (options.oldbrowser) {
                mdepolska.util.oldBrowser();
            }
        }
    };
    mdepolska.init = function(options) {
        if (typeof $ === "undefined") {
            mdepolska.loadScript("https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js?", function() {
                $ = window.jQuery;
                initAll(options);
            });
        } else {
            initAll(options);
        }
    };
    mdepolska.classes = {};
    mdepolska.classes.Typer = function(element) {
        this.prevChar = null;
        this.elem = element;
        this.init();
    };
    mdepolska.classes.Typer.prototype = {
        init: function() {
            var self = this;
            $(this.elem).keydown(function(ev) {
                var typedChar = String.fromCharCode(ev.keyCode);
                if (mdepolska.globals.diacriticChars.indexOf(typedChar) > -1) {
                    self.prevChar = self.getPrevChar();
                    if ("~" === self.prevChar) {
                        if (!ev.shiftKey) {
                            typedChar = typedChar.toLowerCase();
                        }
                        self.replaceChar(typedChar);
                        return false;
                    }
                }
            });
            $(this.elem).addClass("initialized");
        },
        getPrevChar: function() {
            var val = $(this.elem).val();
            if (val.length > 0) {
                return val.substr(this.elem.selectionStart - 1, 1);
            }
            return null;
        },
        replaceChar: function(char) {
            var newChar = this.mapChar(char);
            $(this.elem).val($(this.elem).val().substring(0, this.elem.selectionStart - 1) + newChar);
        },
        mapChar: function(char) {
            console.log(char);
            if (true) {
                switch (char) {
                  case "a":
                    return "ą";

                  case "A":
                    return "Ą";

                  case "C":
                    return "Ć";

                  case "c":
                    return "ć";

                  case "E":
                    return "Ę";

                  case "e":
                    return "ę";

                  case "L":
                    return "Ł";

                  case "l":
                    return "ł";

                  case "N":
                    return "Ń";

                  case "n":
                    return "ń";

                  case "O":
                    return "Ó";

                  case "o":
                    return "ó";

                  case "S":
                    return "Ś";

                  case "s":
                    return "ś";

                  case "X":
                    return "Ź";

                  case "x":
                    return "ź";

                  case "Z":
                    return "Ż";

                  case "z":
                    return "ż";

                  default:
                    return char;
                }
            }
        }
    };
    mdepolska.classes.Blinkenlichten = function(element) {
        this.elem = element;
        this.init();
    };
    mdepolska.classes.Blinkenlichten.prototype = {
        init: function() {
            this.createHtml();
            var self = this;
            $(this.elem).keydown(function(e) {
                var licht = self.getLicht(e);
                self.down(licht);
            }).keyup(function() {
                $("#blinkenlichten ul li").removeClass("active");
            });
        },
        createHtml: function() {
            $(this.elem).after('<div id="blinkenlichten">' + '<ul class="meta">' + '  <li id="tilda">~</li>' + '  <li id="asterisk">*</li>' + "</ul>" + "<ul>" + '  <li id="a">A</li>' + '  <li id="c">C</li>' + '  <li id="e">E</li>' + '  <li id="l">L</li>' + '  <li id="n">N</li>' + '  <li id="o">O</li>' + '  <li id="s">S</li>' + '  <li id="z">Z</li>' + "</ul>" + "</div>");
        },
        getLicht: function(e) {
            var licht = null;
            var charFromCode = String.fromCharCode(e.keyCode);
            if (mdepolska.globals.tildaModKeyCode === e.keyCode && e.shiftKey) {
                licht = $("#blinkenlichten ul li#tilda");
            } else if (mdepolska.globals.asteriskModKeyCode === e.keyCode && e.shiftKey) {
                licht = $("#blinkenlichten ul li#asterisk");
            } else if (mdepolska.globals.diacriticChars.indexOf(charFromCode) > -1) {
                var charLower = charFromCode.toLowerCase();
                licht = $("#blinkenlichten ul li#" + charLower);
            } else {
                return;
            }
            return licht;
        },
        down: function(licht) {
            if (licht !== undefined) {
                licht.addClass("active");
            }
        }
    };
    mdepolska.classes.Decoration = function(element) {
        this.elem = element;
        this.bgColor = "#8DB11F";
        this.init();
    };
    mdepolska.classes.Decoration.prototype = {
        init: function() {
            $(this.elem).css("border", "1px solid " + this.bgColor);
            var top = $(this.elem).offset().top;
            var left = $(this.elem).offset().left + $(this.elem).width() + 1;
            this.createTab(top, left);
        },
        createTab: function(top, left) {
            $(this.elem).before('<div class="polska-decoration" style="' + "position: absolute;" + "background: " + this.bgColor + ";" + "border: 1px solid " + this.bgColor + ";" + "border-top-right-radius: 5px;" + "border-bottom-right-radius: 5px;" + "color: white;" + "cursor: pointer;" + "font-weight: bold;" + "font-family: sans-serif;" + "left: " + left + "px;" + "padding: 2px;" + "top: " + top + 'px;"' + ' title="MDE Polska!"><a href="http://mdworld.nl/cms/content/polska" style="text-decoration: none; color: white;">P</a></div>');
        }
    };
    mdepolska.util = {};
    mdepolska.util.oldBrowser = function() {
        $("textarea.polska").attr("disabled", "disabled").addClass("readonly").addClass("initialized").val("THIS IS AN OLD BROWSER, TYPER IS DISABLED.");
    };
    window.mdepolska = mdepolska;
})(window.jQuery, window.mdepolska || {});
//# sourceMappingURL=popolsku.js.map